Тестовое задание для PHP-программиста
=====================================

psql
 
create database slim_project;

touch .env.local

APP_ENV=dev
DATABASE=postgres://wers:123@127.0.0.1:5432/slim_project

composer install

composer update

./bin/console orm:schema-tool:update  --force

./bin/console fetch:trailers

