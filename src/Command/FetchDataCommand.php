<?php
/**
 * 2019-06-28.
 */

declare(strict_types=1);

namespace App\Command;

use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;
use FeedIo\FeedIo;
use GuzzleHttp\Client;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class FetchDataCommand.
 */
class FetchDataCommand extends Command
{
    private const SOURCE = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss';
    private const LIMIT = 10;

    /**
     * @var string
     */
    protected static $defaultName = 'fetch:trailers';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $source;

    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * FetchDataCommand constructor.
     *
     * @param ClientInterface        $httpClient
     * @param LoggerInterface        $logger
     * @param EntityManagerInterface $em
     * @param string|null            $name
     */
    public function __construct(ClientInterface $httpClient, LoggerInterface $logger, EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->doctrine = $em;
    }

    public function configure(): void
    {
        $this
            ->setDescription('Fetch data from iTunes Movie Trailers')
            ->addArgument('source', InputArgument::OPTIONAL, 'Overwrite source')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->log('Start ' . __CLASS__);

        $source = self::SOURCE;
        if ($input->getArgument('source')) {
            $source = $input->getArgument('source');
        }

        if (!is_string($source)) {
            throw new RuntimeException('Source must be string');
        }
        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Fetch data from %s', $source));

        $feedIo = new FeedIo(new \FeedIo\Adapter\Guzzle\Client(new Client()), $this->logger);

        $result = $feedIo->read($source);

        $this->processFeed($result->getFeed());

        $this->log('End ' . __CLASS__);

        return 0;
    }

    protected function log(string $message)
    {
        $this->logger->info($message);
    }

    /**
     * @param  $feed
     *
     * @throws \Exception
     */
    protected function processFeed($feed): void
    {
        foreach ($feed as $key => $item) {

            $trailer = $this->getMovie((string) $item->getTitle())
                    ->setTitle((string) $item->getTitle())
                    ->setDescription((string) $item->getDescription())
                    ->setLink((string) $item->getLink())
                    ->setPubDate($item->getLastModified())
                    ->setImage($this->parseImage($item->getDescription()));
            $this->doctrine->persist($trailer);

            if (self::LIMIT === $key + 1) {
                break;
            }
        }

        $this->doctrine->flush();
    }

    /**
     * @param string $title
     *
     * @return Movie
     */
    protected function getMovie(string $title): Movie
    {
        $item = $this->doctrine->getRepository(Movie::class)->findOneBy(['title' => $title]);

        if ($item === null) {
            $this->logger->info('Create new Movie', ['title' => $title]);
            $item = new Movie();
        } else {
            $this->logger->info('Move found', ['title' => $title]);
        }

        if (!($item instanceof Movie)) {
            throw new RuntimeException('Wrong type!');
        }

        return $item;
    }

    private function parseImage(string $content)
    {
        $internalErrors = libxml_use_internal_errors(true);
        $xpath = new \DOMXPath(\DOMDocument::loadHTML($content));
        libxml_use_internal_errors($internalErrors);

        return $xpath->evaluate('string(//img/@src)');
    }
}
